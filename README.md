# Projekt do předmětu VIS

## Konfigurace

Konfigurační soubor se nachazí v adresáři **%appdata%/VISSettings**

##### Nastavení typu uložiště

Jako výchozí uložiště je nastaveno ukládání do XML

Pro změnu typu uložiště je ptořeba v adresáři s nastavením vytvořit settings.ini a v něm nakonfigurovat následující položku

    [storage]
    Database=False

##### Nastavení MySQL přihlašovacích údajů

Pro nastavení údajů k MYSQL databázi je potřeba v adresáři s nastavení vytvořit settings.ini a v něm nakonfigurovat následující položky

    [mysql]
    server=localhost
    user=root
    password=root
    database=vis
        

## Možnosti a ovládání datové vrstvy

**Vytvoření instance nějakého modelu**

    Model model = new Model() { 
        name = "Ahojky"
    };

**Uložení modelu do databáze**

    StorageFacade.getInstance().setObject(model).create();

**Načtení dat do modelu**

    Model model2 = new Model();
    StorageFacade.getInstance().setObject(model2).read(1);

**Editace dat modelu a uložení editace do databáze**

    model2.name = "Edit: " + model2.name;
    StorageFacade.getInstance().setObject(pr2).update(pr2.id);

**Smazání modelu s ID 2**

    StorageFacade.getInstance().setObject(new Model()).delete(2);

**Načtení pole modelu podle filteru**
    
    Dictionary<string, string> filter = new Dictionary<string, string>();
    filter.Add("username", "admin");
    filter.Add("password", "admin");
    
    var a = StorageFacade.getInstance().setObject(new User()).read(filter);