﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Layout.Master" AutoEventWireup="true" CodeBehind="NewProject.aspx.cs" Inherits="WebApp.NewProject" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title">Nový projekt</h4>

            <asp:Label ID="errorFlash" runat="server" Text=""></asp:Label>

            <form id="newprojectForm" runat="server">
                <div class="form-group">
                    <label for="projectName">Název projektu</label>
                    <asp:TextBox ID="projectName" class="form-control" runat="server"></asp:TextBox>
                </div>
                <div class="form-group">
                    <label for="projectName">Deadline</label>
                    <asp:TextBox ID="projectDeadline" class="form-control js-datepicker" runat="server"></asp:TextBox>
                    <small id="deadlineHelp" class="form-text text-muted">Datum ve formátu (dd-MM-yyyy)</small>
                </div>
                <div class="form-group">
                    <label for="projectName">Popis</label>
                    <asp:TextBox ID="projectDescription" class="form-control" runat="server" TextMode="MultiLine"></asp:TextBox>
                </div>
                <div class="form-group text-right">
                    <asp:Button ID="saveProject" CssClass="btn btn-primary" runat="server" Text="Uložit" OnClick="saveProject_Click" />
                    <asp:Button ID="exitProject" CssClass="btn btn-danger" runat="server" Text="Vymazat" />
                </div>
            </form>
        </div>
    </div>
</asp:Content>
