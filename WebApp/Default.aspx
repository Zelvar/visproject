﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Layout.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="WebApp.WebForm2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <% if( enabledEdit ) { %>
                
        
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Detail: <%= ((VIS_Project.Models.Project) edit).name %></h4>
                <p class="card-text"><%= ((VIS_Project.Models.Project) edit).description %></p>

                <% if( tasks != null) { %>
                    <% foreach (VIS_Project.Models.Todo todo in tasks)
                       { %>
                        <tr>
                            <td><a href="Tasks.aspx?id=<%=todo.id%>"><%= todo.name %></a></td>
                            <td><%= todo.getProject().name %></td>
                            <td><%= todo.time %></td>
                        </tr>
                    <% } %>
        
                <% } %>

            </div>
        </div>

    <% }else{ %>
        <table class="table">
            <thead>
                <tr>
                    <td>Projekt</td>
                    <td>Priorita</td>
                    <td>Deadline</td>
                </tr>
            </thead>
            <tbody>
                <% if( projects != null) { %>
                    <% foreach (VIS_Project.Models.Project project in projects){ %>
                        <tr>
                            <% if(project.name != ""){ %>
                                <td><a href="?id=<%=project.id%>"><%= project.name %></a></td>
                            <% }else{ %>
                                <td><a href="?id=<%=project.id%>"><i>Neznámý název</i></a></td>
                            <% } %>
                            <td><%= project.priority %></td>
                            <td><%= project.getDeadLine() %></td>
                        </tr>
                    <% } %>
        
                <% } %>
            </tbody>
        </table>
    <% } %>
</asp:Content>
