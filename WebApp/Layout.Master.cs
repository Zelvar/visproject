﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VIS_Project.Classes;
using VIS_Project.Models;

namespace WebApp
{
    public partial class Layout : System.Web.UI.MasterPage
    {
        public User user;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["user"] == null)
            {
                Response.Redirect("Login.aspx");
            }
            else
            {
                this.user = (User)Session["user"];
            }
        }
    }
}