﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VIS_Project.Classes;
using VIS_Project.Models;

namespace WebApp
{
    public partial class NewTask : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            User usr = (User)Session["user"];
            if (User != null)
            {
                var a = usr.getProjects();
                if (a != null)
                {
                    foreach (Project b in a)
                    {
                        ListItem lst = new ListItem(b.name, b.id.ToString());
                        taskProject.Items.Insert(taskProject.Items.Count, lst);
                    }
                }
            }
        }

        protected void saveTask_Click(object sender, EventArgs e)
        {
            this.errorFlash.Text = "";

            if (taskName.Text == "")
            {
                this.errorFlash.Text += "Chybí název ulohy<br/>";
            }
            if (taskDescription.Text == "")
            {
                this.errorFlash.Text += "Chybí popis ulohy<br/>";
            }
            if (taskProject.SelectedItem.Value.ToString() == "")
            {
                this.errorFlash.Text += "Chybí projekt";
            }

            if (this.errorFlash.Text == "")
            {
                try
                {
                    Todo todo = new Todo() { 
                        name = taskName.Text,
                        description = taskDescription.Text,
                        project = int.Parse(taskProject.SelectedItem.Value),
                        time = 0
                    };
                    StorageFacade.getInstance().setObject(todo).create();

                    this.errorFlash.Text = "Úloha byla úspěšně uložena.";
                }
                catch (Exception exc)
                {
                    this.errorFlash.Text = exc.ToString();
                }
            }
        }
    }
}