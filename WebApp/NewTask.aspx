﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Layout.Master" AutoEventWireup="true" CodeBehind="NewTask.aspx.cs" Inherits="WebApp.NewTask" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title">Nová úloha</h4>

            <asp:Label ID="errorFlash" runat="server" Text=""></asp:Label>

            <form id="newprojectForm" runat="server">
                <div class="form-group">
                    <label for="taskName">Název úlohy</label>
                    <asp:TextBox ID="taskName" class="form-control" runat="server"></asp:TextBox>
                </div>
                <div class="form-group">
                    <label for="taskProject">Projekt</label>
                    <asp:DropDownList ID="taskProject" class="form-control" runat="server"></asp:DropDownList>
                </div>
                <div class="form-group">
                    <label for="taskDescription">Popis</label>
                    <asp:TextBox ID="taskDescription" class="form-control" runat="server" TextMode="MultiLine"></asp:TextBox>
                </div>
                <div class="form-group text-right">
                    <asp:Button ID="saveTask" CssClass="btn btn-primary" runat="server" Text="Uložit" OnClick="saveTask_Click" />
                    <asp:Button ID="exitTask" CssClass="btn btn-danger" runat="server" Text="Vymazat" />
                </div>
            </form>
        </div>
    </div>
</asp:Content>
