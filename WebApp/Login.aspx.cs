﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VIS_Project.Models;
using VIS_Project.Classes;

namespace WebApp
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["user"] != null) {
                Response.Redirect("Default.aspx");
            }
        }

        protected void login_Click(object sender, EventArgs e)
        {

            Dictionary<string, string> filter = new Dictionary<string, string>();
            filter.Add("username", username.Text);
            filter.Add("password", password.Text);

            var loginCheck = StorageFacade.getInstance().setObject(new User()).read(filter);
            try
            {
                if (loginCheck != null && loginCheck.Count() > 0)
                {
                    //CREATE SESSION
                    Session["user"] = (User)loginCheck[0];
                    Response.Redirect("Default.aspx");
                }else {
                    this.errorFlash.Text = "Uživatel se zadánymi udaji nebyl nalezen.";
                }
            }
            catch
            {
                this.errorFlash.Text = "Uživatel se zadánymi udaji nebyl nalezen.";
            }
        }
    }
}