﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Layout.Master" AutoEventWireup="true" CodeBehind="Tasks.aspx.cs" Inherits="WebApp.Tasks" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table class="table">
        <thead>
            <tr>
                <td>Úloha</td>
                <td>Projekt</td>
                <td>Strávený čas</td>
            </tr>
        </thead>
        <tbody>
            <% if( tasks != null) { %>
                <% foreach (VIS_Project.Models.Todo todo in tasks)
                   { %>
                    <tr>
                        <td><a href="?id=<%=todo.id%>"><%= todo.name %></a></td>
                        <td><%= todo.getProject().name %></td>
                        <td><%= todo.time %></td>
                    </tr>
                <% } %>
        
            <% } %>
        </tbody>
    </table>
</asp:Content>
