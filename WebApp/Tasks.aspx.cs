﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VIS_Project.Classes;
using VIS_Project.Models;

namespace WebApp
{
    public partial class Tasks : System.Web.UI.Page
    {
        public List<object> tasks;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["user"] != null)
            {
                User usr = (User)Session["user"];
                tasks = usr.getTodos();
            }
        }
    }
}