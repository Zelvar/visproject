﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="WebApp.WebForm1" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Přihlášení do systému</title>
    <link href="style.css" rel="stylesheet" />
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="apple-mobile-web-app-capable" content="yes" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>
</head>
<body>
    <div class="container my-5">
        <div class="offset-md-3 col-md-6 offset-sm-0 col-sm-12">
            <h1>Přihlášení</h1>
            <form id="form1" runat="server">
                <div class="form-group">
                    <asp:Label ID="errorFlash" runat="server" Text=""></asp:Label>
                </div>
        
                <div class="form-group">
                    <label for="username">Uživatel</label>
                    <asp:TextBox ID="username" class="form-control" runat="server"></asp:TextBox>
                </div>
                <div class="form-group">
                    <label for="password">Heslo</label>
                    <asp:TextBox ID="password" class="form-control" type="password" runat="server"></asp:TextBox>
                </div>
                <div class="form-row">
                    <div class="col">
                        <a href="mailto:hlo0043@vsb.cz">Zapomenuté heslo</a>
                    </div>
                    <div class="col text-right">
                        <asp:Button Text="Přihlasit se" class="btn-primary" runat="server" ID="loginButton" OnClick="login_Click" />
                    </div>
                </div>
            </form>
        </div>
    </div>
</body>
</html>
