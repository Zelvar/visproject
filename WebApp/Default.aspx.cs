﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VIS_Project.Models;
using VIS_Project.Classes;

namespace WebApp
{
    public partial class WebForm2 : System.Web.UI.Page
    {
        public List<object> projects;
        public List<object> tasks;
        public bool enabledEdit = false;
        public Project edit = new Project();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["user"] != null)
            {
                User usr = (User)Session["user"];
                projects = usr.getProjects();
            }

            int value;
            if (Request.QueryString["id"] != null && Request.QueryString["id"] != "" && int.TryParse(Request.QueryString["id"], out value))
            {
                enabledEdit = true;
                edit.id = value;
                
                StorageFacade.getInstance().setObject(edit).read(edit.id);
                this.tasks = edit.getTodos();
            }
        }
    }
}