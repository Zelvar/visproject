﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VIS_Project.Models;
using VIS_Project.Classes;
using System.IO;
using System.Globalization;

namespace WebApp
{
    public partial class NewProject : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        { }

        protected void saveProject_Click(object sender, EventArgs e)
        {
            this.errorFlash.Text = "";

            if (projectName.Text == "")
            {
                this.errorFlash.Text += "Chybí název projektu<br/>";
            }
            if (projectDescription.Text == "")
            {
                this.errorFlash.Text += "Chybí popis projektu<br/>";
            }
            if (projectDeadline.Text == "")
            {
                this.errorFlash.Text += "Chybí deadline projektu";
            }
            
            if (this.errorFlash.Text == "")
            {
                try
                {
                    DateTime time = DateTime.ParseExact(projectDeadline.Text + " 00:00", "dd-MM-yyyy HH:mm", CultureInfo.InvariantCulture);
                    var epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
                    Project projectToSave = new Project()
                    {
                        name = projectName.Text,
                        description = projectDescription.Text,
                        deadlinetimestamp = time.ToString()
                    };

                    User usr = (User)Session["user"];
                    usr.createProject(projectToSave);
                    this.errorFlash.Text = "Projekt byl úspěšně uložen.";
                }
                catch (Exception exc)
                {
                    this.errorFlash.Text = exc.ToString();
                }
            }
        }
    }
}