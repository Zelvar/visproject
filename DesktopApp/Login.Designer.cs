﻿namespace DesktopApp
{
    partial class Login
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Login));
            this.loginInput = new System.Windows.Forms.TextBox();
            this.passwordInput = new System.Windows.Forms.TextBox();
            this.buttonLogin = new System.Windows.Forms.Button();
            this.loginLabel = new System.Windows.Forms.Label();
            this.lostPassword = new System.Windows.Forms.LinkLabel();
            this.loginError = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // loginInput
            // 
            this.loginInput.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.loginInput.Location = new System.Drawing.Point(12, 56);
            this.loginInput.Name = "loginInput";
            this.loginInput.Size = new System.Drawing.Size(260, 29);
            this.loginInput.TabIndex = 0;
            this.loginInput.Text = "admin";
            this.loginInput.Enter += new System.EventHandler(this.loginInput_Enter);
            // 
            // passwordInput
            // 
            this.passwordInput.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.passwordInput.Location = new System.Drawing.Point(13, 97);
            this.passwordInput.Name = "passwordInput";
            this.passwordInput.PasswordChar = '*';
            this.passwordInput.Size = new System.Drawing.Size(259, 29);
            this.passwordInput.TabIndex = 1;
            this.passwordInput.Text = "admin";
            this.passwordInput.Enter += new System.EventHandler(this.loginInput_Enter);
            // 
            // buttonLogin
            // 
            this.buttonLogin.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonLogin.Location = new System.Drawing.Point(153, 132);
            this.buttonLogin.Name = "buttonLogin";
            this.buttonLogin.Size = new System.Drawing.Size(119, 31);
            this.buttonLogin.TabIndex = 2;
            this.buttonLogin.Text = "Přihlasit se";
            this.buttonLogin.UseVisualStyleBackColor = true;
            this.buttonLogin.Click += new System.EventHandler(this.buttonLogin_Click);
            // 
            // loginLabel
            // 
            this.loginLabel.AutoSize = true;
            this.loginLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.loginLabel.Location = new System.Drawing.Point(10, 9);
            this.loginLabel.Name = "loginLabel";
            this.loginLabel.Size = new System.Drawing.Size(77, 20);
            this.loginLabel.TabIndex = 3;
            this.loginLabel.Text = "Přihlašení";
            // 
            // lostPassword
            // 
            this.lostPassword.AutoSize = true;
            this.lostPassword.Location = new System.Drawing.Point(11, 143);
            this.lostPassword.Name = "lostPassword";
            this.lostPassword.Size = new System.Drawing.Size(95, 13);
            this.lostPassword.TabIndex = 4;
            this.lostPassword.TabStop = true;
            this.lostPassword.Text = "Zapomenuté heslo";
            this.lostPassword.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lostPassword_LinkClicked);
            // 
            // loginError
            // 
            this.loginError.AutoSize = true;
            this.loginError.ForeColor = System.Drawing.Color.Red;
            this.loginError.Location = new System.Drawing.Point(14, 37);
            this.loginError.Name = "loginError";
            this.loginError.Size = new System.Drawing.Size(0, 13);
            this.loginError.TabIndex = 5;
            // 
            // Login
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 175);
            this.Controls.Add(this.loginError);
            this.Controls.Add(this.lostPassword);
            this.Controls.Add(this.loginLabel);
            this.Controls.Add(this.buttonLogin);
            this.Controls.Add(this.passwordInput);
            this.Controls.Add(this.loginInput);
            this.DoubleBuffered = true;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(300, 214);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(300, 214);
            this.Name = "Login";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Přihlášení do systému";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox loginInput;
        private System.Windows.Forms.TextBox passwordInput;
        private System.Windows.Forms.Button buttonLogin;
        private System.Windows.Forms.Label loginLabel;
        private System.Windows.Forms.LinkLabel lostPassword;
        private System.Windows.Forms.Label loginError;
    }
}

