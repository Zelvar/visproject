﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using VIS_Project.Classes;
using VIS_Project.Models;

namespace DesktopApp
{
    public partial class TaskDetail : Form
    {
        Todo task;
        public TaskDetail(Todo task)
        {
            InitializeComponent();
            this.task = task;

            this.Text = this.Text.Replace("@taskName", this.task.name);
            this.taskName.Text = this.task.name;
            this.richTextBox1.Text = this.task.description;
        }
    }
}
