﻿namespace DesktopApp
{
    partial class AddTask
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AddTask));
            this.heading = new System.Windows.Forms.Label();
            this.addTaskButton = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.taskName = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.taskDescription = new System.Windows.Forms.TextBox();
            this.errorFlash = new System.Windows.Forms.Label();
            this.selectProject = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // heading
            // 
            this.heading.AutoSize = true;
            this.heading.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.heading.Location = new System.Drawing.Point(12, 9);
            this.heading.Name = "heading";
            this.heading.Size = new System.Drawing.Size(106, 24);
            this.heading.TabIndex = 0;
            this.heading.Text = "Nová úloha";
            // 
            // addTaskButton
            // 
            this.addTaskButton.Location = new System.Drawing.Point(347, 151);
            this.addTaskButton.Name = "addTaskButton";
            this.addTaskButton.Size = new System.Drawing.Size(75, 23);
            this.addTaskButton.TabIndex = 1;
            this.addTaskButton.Text = "Uložit";
            this.addTaskButton.UseVisualStyleBackColor = true;
            this.addTaskButton.Click += new System.EventHandler(this.addTaskButton_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 37);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(38, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Název";
            // 
            // taskName
            // 
            this.taskName.Location = new System.Drawing.Point(16, 53);
            this.taskName.Name = "taskName";
            this.taskName.Size = new System.Drawing.Size(209, 20);
            this.taskName.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(16, 76);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(33, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Popis";
            // 
            // taskDescription
            // 
            this.taskDescription.Location = new System.Drawing.Point(16, 93);
            this.taskDescription.Multiline = true;
            this.taskDescription.Name = "taskDescription";
            this.taskDescription.Size = new System.Drawing.Size(406, 52);
            this.taskDescription.TabIndex = 5;
            // 
            // errorFlash
            // 
            this.errorFlash.AutoSize = true;
            this.errorFlash.Location = new System.Drawing.Point(125, 19);
            this.errorFlash.Name = "errorFlash";
            this.errorFlash.Size = new System.Drawing.Size(0, 13);
            this.errorFlash.TabIndex = 6;
            // 
            // selectProject
            // 
            this.selectProject.FormattingEnabled = true;
            this.selectProject.Location = new System.Drawing.Point(232, 53);
            this.selectProject.Name = "selectProject";
            this.selectProject.Size = new System.Drawing.Size(190, 21);
            this.selectProject.TabIndex = 7;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(229, 37);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(40, 13);
            this.label3.TabIndex = 8;
            this.label3.Text = "Projekt";
            // 
            // AddTask
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(434, 186);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.selectProject);
            this.Controls.Add(this.errorFlash);
            this.Controls.Add(this.taskDescription);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.taskName);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.addTaskButton);
            this.Controls.Add(this.heading);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximumSize = new System.Drawing.Size(450, 225);
            this.MinimumSize = new System.Drawing.Size(450, 225);
            this.Name = "AddTask";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Přidat úlohu";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label heading;
        private System.Windows.Forms.Button addTaskButton;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox taskName;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox taskDescription;
        private System.Windows.Forms.Label errorFlash;
        private System.Windows.Forms.ComboBox selectProject;
        private System.Windows.Forms.Label label3;
    }
}