﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using VIS_Project.Classes;
using VIS_Project.Models;

namespace DesktopApp
{
    public partial class ProjectDetail : Form
    {
        private Project project;
        private User user;

        private int totalTime = 0;
        private int usersCount = 0;

        private string buttonText1;
        private string buttonText2;

        public ProjectDetail(Project project, User user)
        {            
            InitializeComponent();

            buttonText1 = totalTimeText.Text;
            buttonText2 = totalUsersCountText.Text;

            this.project = project;
            this.user = user;
            loadForm();
            try
            {
                var users = project.getUsers();

                DataGridViewRow rowd;
                usersGridView.Rows.Clear();
                usersGridView.Rows.Add();
                totalTime = 0;

                foreach (User a in users)
                {
                    rowd = (DataGridViewRow)usersGridView.Rows[0].Clone();

                    rowd.Cells[0].Value = a.id;
                    rowd.Cells[1].Value = a.username;

                    usersGridView.Rows.Add(rowd);
                }

                usersGridView.Rows.RemoveAt(0);
            } catch { }
        }

        public void loadForm() {
            this.Text = this.Text.Replace("@projectName", this.project.name + " (ID:" + this.project.id + ")");
            
            this.projectName.Text = this.project.name;

            try
            {
                List<object> todos = project.getTodos();
                DataGridViewRow rowd;
                tasksList.Rows.Clear();
                tasksList.Rows.Add();
                totalTime = 0;

                foreach (Todo a in todos)
                {
                    rowd = (DataGridViewRow)tasksList.Rows[0].Clone();

                    rowd.Cells[0].Value = a.id;
                    rowd.Cells[1].Value = a.name;
                    rowd.Cells[2].Value = a.getProject().name;
                    rowd.Cells[3].Value = a.time;

                    totalTime += a.time;

                    tasksList.Rows.Add(rowd);
                }

                tasksList.Rows.RemoveAt(0);
            }
            catch { }
            tasksList.CellDoubleClick += delegate (object s, DataGridViewCellEventArgs a)
            {
                this.openDetailTask(s, a);
            };

            /* Nastavení textu labelu */
            totalTimeText.Text = buttonText1;
            totalTimeText.Text = totalTimeText.Text.Replace("@totalTime", this.totalTime.ToString() + " hodin");
            totalUsersCountText.Text = buttonText2;
            totalUsersCountText.Text = totalUsersCountText.Text.Replace("@totalUsersCount", this.usersCount.ToString());
        }

        private void settingsLabel_Click(object sender, EventArgs e)
        {
            ProjectSettings ps = new ProjectSettings(this.project);
            if (ps.ShowDialog(this) == DialogResult.OK) {
                this.loadForm();
            }
        }

        private void openDetailTask(object sender, DataGridViewCellEventArgs e)
        {
            DataGridView a = (DataGridView)sender;
            int id = int.Parse(a.Rows[e.RowIndex].Cells[0].Value.ToString());
            if (id > 0)
            {
                TaskDetail td = new TaskDetail(this.user.getTask(id));
                td.ShowDialog();
            }
            else
            {
                MessageBox.Show("Nastala chyba.", "Error");
            }
        }
    }
}
