﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using VIS_Project.Classes;
using VIS_Project.Models;

namespace DesktopApp
{
    public partial class Login : Form
    {
        ProjectsWindow pw = null;
        public Login()
        {
            InitializeComponent();
        }

        private void lostPassword_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            System.Diagnostics.Process.Start("mailto:hlo0043@vsb.cz");
        }

        private void buttonLogin_Click(object sender, EventArgs e)
        {
            if (loginInput.Text != "" && passwordInput.Text != "")
            {
                Dictionary<string, string> filter = new Dictionary<string, string>();
                filter.Add("username", loginInput.Text);
                filter.Add("password", passwordInput.Text);
                
                var loginCheck = StorageFacade.getInstance().setObject(new User()).read(filter);
                try
                {
                    if (loginCheck != null && loginCheck.Count() > 0 && pw == null || pw.Visible == false)
                    {
                        this.Hide();
                        pw = new ProjectsWindow((User)loginCheck[0]);
                        pw.FormClosed += delegate { this.Close(); };
                        pw.Show();
                    }
                }
                catch {
                    this.loginError.Text = "Uživatel se zadánymi udaji nebyl nalezen.";
                }
            }
            else {
                this.loginError.Text = "Nebylo vyplněno některé z požadovaných polí";
            }
        }

        private void loginInput_Enter(object sender, EventArgs e)
        {
            this.loginError.Text = "";
        }
    }
}
