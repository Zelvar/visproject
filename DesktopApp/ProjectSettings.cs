﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using VIS_Project.Models;
using VIS_Project.Classes;

namespace DesktopApp
{
    public partial class ProjectSettings : Form
    {
        Project project;
        public ProjectSettings(Project project)
        {
            InitializeComponent();
            this.project = project;

            projectNameTextBox.Text = project.name;
            projectDescriptionRichBox.Text = project.description;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Je možné, že provedené změny nebudou uloženy", "Opravdu chcete odejít?", MessageBoxButtons.OKCancel) == System.Windows.Forms.DialogResult.OK)
            {
                this.Close();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.project.name = projectNameTextBox.Text;
            this.project.description = projectDescriptionRichBox.Text;
            try
            {
                project.save();
                MessageBox.Show("Projekt byl uspěšně upraven.", "Uspěšně uloženo");
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
            catch (Exception ex){
                MessageBox.Show("Uložení se nezdařilo" + ex.ToString());
            }
        }
    }
}
