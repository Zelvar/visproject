﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using VIS_Project.Classes;
using VIS_Project.Models;
using System.Diagnostics;

namespace DesktopApp
{
    public partial class ProjectsWindow : Form
    {
        User user = null;
        List<object> projects = new List<object>();
        List<object> todos = new List<object>();

        public ProjectsWindow(User usr)
        {
            InitializeComponent();

            //Načtení uživatele
            this.user = usr;
            this.Text = this.Text.Replace("@user", "Uživatel: " + this.user.username);
            reloadProjects();

            projectList.CellDoubleClick += delegate(object s, DataGridViewCellEventArgs a)
            {
                this.openDetailProject(s, a);
            };
            tasksList.CellDoubleClick += delegate(object s, DataGridViewCellEventArgs a)
            {
                this.openDetailTask(s, a);
            };

            if(this.user.getPosition() == Position.Admin)
            {
                this.profileImage.Image = Properties.Resources.admin;
            }
            else
            {
                this.profileImage.Image = Properties.Resources.user;

            }
        }

        private void ProjectsWindow_Load(object sender, EventArgs e)
        {

        }

        private void buttonProjects_Click(object sender, EventArgs e)
        {
            tabControl.SelectedIndex = 0;
            reloadProjects();

        }

        private void button1_Click(object sender, EventArgs e)
        {
            tabControl.SelectedIndex = 1;
            reloadTasks();
        }

        private void addTaskButton_Click(object sender, EventArgs e)
        {
            AddTask task = new AddTask(user);
            task.ShowDialog();
            reloadTasks();
        }

        private void addProjectButton_Click(object sender, EventArgs e)
        {
            AddProject project = new AddProject(user);
            project.ShowDialog();
            reloadProjects();
        }

        private void reloadProjects()
        {
            projects = user.getProjects(true);
            DataGridViewRow rowd;
            projectList.Rows.Clear();
            projectList.Rows.Add();
            
            foreach (Project a in projects)
            {
                if (a.id > 0 && a.description != null) {
                    rowd = (DataGridViewRow)projectList.Rows[0].Clone();

                    rowd.Cells[0].Value = a.id;
                    rowd.Cells[1].Value = a.name;
                    if (a.getDeadLine().Year != 1970)
                    {
                        rowd.Cells[2].Value = a.deadlinetimestamp;
                    }
                    else {
                        rowd.Cells[2].Value = "Deadline nenastaven";
                    }
                    rowd.Cells[3].Value = a.priority.ToString();

                    projectList.Rows.Add(rowd);
                }
            }

            projectList.Rows.RemoveAt(0);
        }

        private void reloadTasks()
        {
            todos = user.getTodos(true);
            DataGridViewRow rowd;
            tasksList.Rows.Clear();
            tasksList.Rows.Add();

            foreach (Todo a in todos)
            {
                rowd = (DataGridViewRow)tasksList.Rows[0].Clone();

                rowd.Cells[0].Value = a.id;
                rowd.Cells[1].Value = a.name;
                rowd.Cells[2].Value = a.getProject().name;

                tasksList.Rows.Add(rowd);
            }

            tasksList.Rows.RemoveAt(0);
        }

        private void openDetailProject(object sender, DataGridViewCellEventArgs e)
        {
            DataGridView a = (DataGridView)sender;
            int id = int.Parse(a.Rows[e.RowIndex].Cells[0].Value.ToString());
            if (id > 0)
            {
                ProjectDetail pw = new ProjectDetail(this.user.getProject(id), this.user);
                pw.ShowDialog();
            }
            else
            {
                MessageBox.Show("Nastala chyba.");
            }
        }

        private void openDetailTask(object sender, DataGridViewCellEventArgs e)
        {
            DataGridView a = (DataGridView)sender;
            int id = int.Parse(a.Rows[e.RowIndex].Cells[0].Value.ToString());
            if (id > 0)
            {
                TaskDetail td = new TaskDetail(this.user.getTask(id));
                td.ShowDialog();
            }
            else
            {
                MessageBox.Show("Nastala chyba.", "Error");
            }
        }
    }
}
