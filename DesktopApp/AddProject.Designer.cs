﻿namespace DesktopApp
{
    partial class AddProject
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AddProject));
            this.heading = new System.Windows.Forms.Label();
            this.addProjectButton = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.projectName = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.projectDescription = new System.Windows.Forms.TextBox();
            this.datetimeProjectLabel = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.errorFlash = new System.Windows.Forms.Label();
            this.deadlinePicker = new System.Windows.Forms.DateTimePicker();
            this.SuspendLayout();
            // 
            // heading
            // 
            this.heading.AutoSize = true;
            this.heading.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.heading.Location = new System.Drawing.Point(12, 9);
            this.heading.Name = "heading";
            this.heading.Size = new System.Drawing.Size(114, 24);
            this.heading.TabIndex = 1;
            this.heading.Text = "Nový projekt";
            // 
            // addProjectButton
            // 
            this.addProjectButton.Location = new System.Drawing.Point(347, 151);
            this.addProjectButton.Name = "addProjectButton";
            this.addProjectButton.Size = new System.Drawing.Size(75, 23);
            this.addProjectButton.TabIndex = 2;
            this.addProjectButton.Text = "Uložit";
            this.addProjectButton.UseVisualStyleBackColor = true;
            this.addProjectButton.Click += new System.EventHandler(this.addProjectButton_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 33);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(38, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Název";
            // 
            // projectName
            // 
            this.projectName.Location = new System.Drawing.Point(16, 50);
            this.projectName.Name = "projectName";
            this.projectName.Size = new System.Drawing.Size(406, 20);
            this.projectName.TabIndex = 4;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 77);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(33, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Popis";
            // 
            // projectDescription
            // 
            this.projectDescription.Location = new System.Drawing.Point(15, 94);
            this.projectDescription.Multiline = true;
            this.projectDescription.Name = "projectDescription";
            this.projectDescription.Size = new System.Drawing.Size(211, 80);
            this.projectDescription.TabIndex = 6;
            // 
            // datetimeProjectLabel
            // 
            this.datetimeProjectLabel.AutoSize = true;
            this.datetimeProjectLabel.Location = new System.Drawing.Point(234, 77);
            this.datetimeProjectLabel.Name = "datetimeProjectLabel";
            this.datetimeProjectLabel.Size = new System.Drawing.Size(49, 13);
            this.datetimeProjectLabel.TabIndex = 8;
            this.datetimeProjectLabel.Text = "Deadline";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label3.Location = new System.Drawing.Point(235, 117);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(120, 9);
            this.label3.TabIndex = 9;
            this.label3.Text = "Datum ve formátu DD-MM-YYYY";
            // 
            // errorFlash
            // 
            this.errorFlash.AutoSize = true;
            this.errorFlash.Location = new System.Drawing.Point(133, 19);
            this.errorFlash.Name = "errorFlash";
            this.errorFlash.Size = new System.Drawing.Size(0, 13);
            this.errorFlash.TabIndex = 10;
            // 
            // deadlinePicker
            // 
            this.deadlinePicker.Location = new System.Drawing.Point(237, 94);
            this.deadlinePicker.Name = "deadlinePicker";
            this.deadlinePicker.Size = new System.Drawing.Size(185, 20);
            this.deadlinePicker.TabIndex = 11;
            // 
            // AddProject
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(434, 186);
            this.Controls.Add(this.deadlinePicker);
            this.Controls.Add(this.errorFlash);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.datetimeProjectLabel);
            this.Controls.Add(this.projectDescription);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.projectName);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.addProjectButton);
            this.Controls.Add(this.heading);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximumSize = new System.Drawing.Size(450, 225);
            this.MinimumSize = new System.Drawing.Size(450, 225);
            this.Name = "AddProject";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Přidat projekt";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label heading;
        private System.Windows.Forms.Button addProjectButton;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox projectName;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox projectDescription;
        private System.Windows.Forms.Label datetimeProjectLabel;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label errorFlash;
        private System.Windows.Forms.DateTimePicker deadlinePicker;
    }
}