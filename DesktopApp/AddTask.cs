﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using VIS_Project.Classes;
using VIS_Project.Models;

namespace DesktopApp
{    public partial class AddTask : Form
    {
        public AddTask(User usr)
        {
            InitializeComponent();
            var datasource = new List<projectList>();
            foreach (Project a in usr.getProjects()) {
                datasource.Add(new projectList() { Name = a.name, Value = a.id.ToString() } );
            }
            selectProject.DataSource = datasource;
            selectProject.DisplayMember = "Name";
            selectProject.ValueMember = "Value";
        }

        private void addTaskButton_Click(object sender, EventArgs e)
        {
            errorFlash.Text = "";
            if (taskName.Text == "")
                errorFlash.Text += "\nChybí název úlohy";
            if (taskDescription.Text == "")
                errorFlash.Text += "\nChybí popis úlohy";
            if (selectProject.Text == "")
                errorFlash.Text += "\nNebyl vybrán projekt";

            if (errorFlash.Text == "") {
                try
                {
                    StorageFacade.getInstance().setObject(new Todo()
                    {
                        name = taskName.Text,
                        project = int.Parse(selectProject.SelectedValue.ToString()),
                        description = taskDescription.Text,
                        time = 0
                    }).create();

                    errorFlash.Text = "Data byly úspěšně uloženy.";
                    MessageBox.Show(errorFlash.Text);
                    errorFlash.Text = "";
                    this.Close();
                }
                catch (Exception exc) {
                    errorFlash.Text = "Data se nepodařilo uložit."+exc.ToString();
                }
            }
        }
    }


    public class projectList
    {
        public string Name { get; set; }
        public string Value { get; set; }
    }
}
