﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using VIS_Project.Classes;
using VIS_Project.Models;
using System.Globalization;

namespace DesktopApp
{
    public partial class AddProject : Form
    {
        public AddProject(User usr)
        {
            InitializeComponent();
        }

        private void addProjectButton_Click(object sender, EventArgs e)
        {
            errorFlash.Text = "";

            if (projectName.Text == "") errorFlash.Text += "\nChybí název projektu";
            if (projectDescription.Text == "") errorFlash.Text += "\nChybí popi projektu";
            if (deadlinePicker.Value > DateTime.Now) errorFlash.Text += "\nChybí deadline projektu";

            if (errorFlash.Text == "")
            {
                try
                {
                    DateTime time = deadlinePicker.Value;
                    var epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
                    StorageFacade.getInstance().setObject(new Project() {
                        name = projectName.Text,
                        description = projectDescription.Text,
                        deadlinetimestamp = deadlinePicker.Value.ToString()
                    }).create();

                    errorFlash.Text = "Data byly úspěšně uloženy.";
                    MessageBox.Show(errorFlash.Text);
                    errorFlash.Text = "";
                    this.Close();
                }
                catch (Exception exc)
                {
                    errorFlash.Text = "Data se nepodařilo uložit." + exc.ToString();
                    MessageBox.Show(errorFlash.Text);
                }
            }
        }
    }
}
