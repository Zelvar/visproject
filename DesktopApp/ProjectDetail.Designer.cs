﻿namespace DesktopApp
{
    partial class ProjectDetail
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ProjectDetail));
            this.projectName = new System.Windows.Forms.Label();
            this.settingsLabel = new System.Windows.Forms.Label();
            this.tasksList = new System.Windows.Forms.DataGridView();
            this.taskID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.taskName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.projectNameTask = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.taskTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.todoHeader = new System.Windows.Forms.Label();
            this.userHeaderText = new System.Windows.Forms.Label();
            this.usersGridView = new System.Windows.Forms.DataGridView();
            this.totalTimeText = new System.Windows.Forms.Label();
            this.totalUsersCountText = new System.Windows.Forms.Label();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.tasksList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.usersGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // projectName
            // 
            this.projectName.AutoSize = true;
            this.projectName.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.projectName.Location = new System.Drawing.Point(13, 13);
            this.projectName.Name = "projectName";
            this.projectName.Size = new System.Drawing.Size(195, 31);
            this.projectName.TabIndex = 0;
            this.projectName.Text = "@projectName";
            // 
            // settingsLabel
            // 
            this.settingsLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.settingsLabel.AutoSize = true;
            this.settingsLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.settingsLabel.Location = new System.Drawing.Point(941, 9);
            this.settingsLabel.Name = "settingsLabel";
            this.settingsLabel.Size = new System.Drawing.Size(66, 13);
            this.settingsLabel.TabIndex = 1;
            this.settingsLabel.Text = "Nastavení";
            this.settingsLabel.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.settingsLabel.Click += new System.EventHandler(this.settingsLabel_Click);
            // 
            // tasksList
            // 
            this.tasksList.AllowUserToAddRows = false;
            this.tasksList.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tasksList.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.tasksList.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells;
            this.tasksList.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.EnableAlwaysIncludeHeaderText;
            this.tasksList.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.taskID,
            this.taskName,
            this.projectNameTask,
            this.taskTime});
            this.tasksList.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.tasksList.Location = new System.Drawing.Point(19, 383);
            this.tasksList.Name = "tasksList";
            this.tasksList.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Sunken;
            this.tasksList.Size = new System.Drawing.Size(988, 166);
            this.tasksList.TabIndex = 1;
            // 
            // taskID
            // 
            this.taskID.HeaderText = "ID";
            this.taskID.Name = "taskID";
            this.taskID.ReadOnly = true;
            this.taskID.Visible = false;
            // 
            // taskName
            // 
            this.taskName.HeaderText = "Úloha";
            this.taskName.Name = "taskName";
            // 
            // projectNameTask
            // 
            this.projectNameTask.HeaderText = "Projekt";
            this.projectNameTask.Name = "projectNameTask";
            // 
            // taskTime
            // 
            this.taskTime.HeaderText = "Počet odpracovaných hodin";
            this.taskTime.Name = "taskTime";
            // 
            // todoHeader
            // 
            this.todoHeader.AutoSize = true;
            this.todoHeader.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.todoHeader.Location = new System.Drawing.Point(12, 349);
            this.todoHeader.Name = "todoHeader";
            this.todoHeader.Size = new System.Drawing.Size(84, 31);
            this.todoHeader.TabIndex = 2;
            this.todoHeader.Text = "Úlohy";
            // 
            // userHeaderText
            // 
            this.userHeaderText.AutoSize = true;
            this.userHeaderText.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.userHeaderText.Location = new System.Drawing.Point(13, 117);
            this.userHeaderText.Name = "userHeaderText";
            this.userHeaderText.Size = new System.Drawing.Size(236, 31);
            this.userHeaderText.TabIndex = 3;
            this.userHeaderText.Text = "Přiřazení uživatelé";
            // 
            // usersGridView
            // 
            this.usersGridView.AllowUserToAddRows = false;
            this.usersGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.usersGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.usersGridView.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells;
            this.usersGridView.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.EnableAlwaysIncludeHeaderText;
            this.usersGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2});
            this.usersGridView.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.usersGridView.Location = new System.Drawing.Point(19, 151);
            this.usersGridView.Name = "usersGridView";
            this.usersGridView.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Sunken;
            this.usersGridView.Size = new System.Drawing.Size(988, 195);
            this.usersGridView.TabIndex = 4;
            // 
            // totalTimeText
            // 
            this.totalTimeText.AutoSize = true;
            this.totalTimeText.Location = new System.Drawing.Point(16, 53);
            this.totalTimeText.Name = "totalTimeText";
            this.totalTimeText.Size = new System.Drawing.Size(224, 13);
            this.totalTimeText.TabIndex = 5;
            this.totalTimeText.Text = "Celkový čas strávený na projektu: @totalTime";
            // 
            // totalUsersCountText
            // 
            this.totalUsersCountText.AutoSize = true;
            this.totalUsersCountText.Location = new System.Drawing.Point(16, 73);
            this.totalUsersCountText.Name = "totalUsersCountText";
            this.totalUsersCountText.Size = new System.Drawing.Size(316, 13);
            this.totalUsersCountText.TabIndex = 6;
            this.totalUsersCountText.Text = "Počet uživatelů přiřazených k tomuto projektu: @totalUsersCount";
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.HeaderText = "ID";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.Visible = false;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.HeaderText = "Uživatelské jméno";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            // 
            // ProjectDetail
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientSize = new System.Drawing.Size(1019, 561);
            this.Controls.Add(this.totalUsersCountText);
            this.Controls.Add(this.totalTimeText);
            this.Controls.Add(this.usersGridView);
            this.Controls.Add(this.userHeaderText);
            this.Controls.Add(this.todoHeader);
            this.Controls.Add(this.tasksList);
            this.Controls.Add(this.settingsLabel);
            this.Controls.Add(this.projectName);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(1035, 600);
            this.MinimumSize = new System.Drawing.Size(1035, 600);
            this.Name = "ProjectDetail";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Projekt: @projectName - Editace projektu";
            ((System.ComponentModel.ISupportInitialize)(this.tasksList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.usersGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label projectName;
        private System.Windows.Forms.Label settingsLabel;
        private System.Windows.Forms.DataGridView tasksList;
        private System.Windows.Forms.Label todoHeader;
        private System.Windows.Forms.DataGridViewTextBoxColumn taskID;
        private System.Windows.Forms.DataGridViewTextBoxColumn taskName;
        private System.Windows.Forms.DataGridViewTextBoxColumn projectNameTask;
        private System.Windows.Forms.DataGridViewTextBoxColumn taskTime;
        private System.Windows.Forms.Label userHeaderText;
        private System.Windows.Forms.DataGridView usersGridView;
        private System.Windows.Forms.Label totalTimeText;
        private System.Windows.Forms.Label totalUsersCountText;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
    }
}