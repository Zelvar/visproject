﻿namespace DesktopApp
{
    partial class ProjectsWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ProjectsWindow));
            this.leftControlPanel = new System.Windows.Forms.Panel();
            this.button2 = new System.Windows.Forms.Button();
            this.buttonsPanelControls = new System.Windows.Forms.Panel();
            this.button1 = new System.Windows.Forms.Button();
            this.addProject = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.tabControl = new System.Windows.Forms.TabControl();
            this.projectTab = new System.Windows.Forms.TabPage();
            this.addProjectButton = new System.Windows.Forms.Button();
            this.projectList = new System.Windows.Forms.DataGridView();
            this.id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Projekt = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.deadline = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.priority = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabTasks = new System.Windows.Forms.TabPage();
            this.addTaskButton = new System.Windows.Forms.Button();
            this.tasksList = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.task = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.profileImage = new System.Windows.Forms.PictureBox();
            this.leftControlPanel.SuspendLayout();
            this.buttonsPanelControls.SuspendLayout();
            this.panel2.SuspendLayout();
            this.tabControl.SuspendLayout();
            this.projectTab.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.projectList)).BeginInit();
            this.tabTasks.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tasksList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.profileImage)).BeginInit();
            this.SuspendLayout();
            // 
            // leftControlPanel
            // 
            this.leftControlPanel.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("leftControlPanel.BackgroundImage")));
            this.leftControlPanel.Controls.Add(this.profileImage);
            this.leftControlPanel.Controls.Add(this.button2);
            this.leftControlPanel.Controls.Add(this.buttonsPanelControls);
            this.leftControlPanel.Dock = System.Windows.Forms.DockStyle.Left;
            this.leftControlPanel.Location = new System.Drawing.Point(0, 0);
            this.leftControlPanel.Name = "leftControlPanel";
            this.leftControlPanel.Size = new System.Drawing.Size(225, 661);
            this.leftControlPanel.TabIndex = 0;
            // 
            // button2
            // 
            this.button2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.button2.BackColor = System.Drawing.Color.Transparent;
            this.button2.Font = new System.Drawing.Font("Microsoft YaHei UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.button2.ForeColor = System.Drawing.SystemColors.Desktop;
            this.button2.Location = new System.Drawing.Point(3, 624);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(216, 25);
            this.button2.TabIndex = 2;
            this.button2.Text = "Nastavení";
            this.button2.UseVisualStyleBackColor = false;
            // 
            // buttonsPanelControls
            // 
            this.buttonsPanelControls.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonsPanelControls.BackColor = System.Drawing.Color.Transparent;
            this.buttonsPanelControls.Controls.Add(this.button1);
            this.buttonsPanelControls.Controls.Add(this.addProject);
            this.buttonsPanelControls.Location = new System.Drawing.Point(0, 190);
            this.buttonsPanelControls.Name = "buttonsPanelControls";
            this.buttonsPanelControls.Size = new System.Drawing.Size(223, 266);
            this.buttonsPanelControls.TabIndex = 0;
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.Transparent;
            this.button1.Font = new System.Drawing.Font("Microsoft YaHei UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.button1.ForeColor = System.Drawing.SystemColors.Desktop;
            this.button1.Location = new System.Drawing.Point(4, 43);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(216, 34);
            this.button1.TabIndex = 1;
            this.button1.Text = "Úlohy";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // addProject
            // 
            this.addProject.BackColor = System.Drawing.Color.Transparent;
            this.addProject.Font = new System.Drawing.Font("Microsoft YaHei UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.addProject.ForeColor = System.Drawing.SystemColors.Desktop;
            this.addProject.Location = new System.Drawing.Point(3, 3);
            this.addProject.Name = "addProject";
            this.addProject.Size = new System.Drawing.Size(216, 34);
            this.addProject.TabIndex = 0;
            this.addProject.Text = "Projekty";
            this.addProject.UseVisualStyleBackColor = false;
            this.addProject.Click += new System.EventHandler(this.buttonProjects_Click);
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.panel2.Controls.Add(this.tabControl);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(225, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(759, 661);
            this.panel2.TabIndex = 1;
            // 
            // tabControl
            // 
            this.tabControl.Appearance = System.Windows.Forms.TabAppearance.FlatButtons;
            this.tabControl.Controls.Add(this.projectTab);
            this.tabControl.Controls.Add(this.tabTasks);
            this.tabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl.ItemSize = new System.Drawing.Size(0, 1);
            this.tabControl.Location = new System.Drawing.Point(0, 0);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(759, 661);
            this.tabControl.SizeMode = System.Windows.Forms.TabSizeMode.Fixed;
            this.tabControl.TabIndex = 0;
            // 
            // projectTab
            // 
            this.projectTab.Controls.Add(this.addProjectButton);
            this.projectTab.Controls.Add(this.projectList);
            this.projectTab.Location = new System.Drawing.Point(4, 5);
            this.projectTab.Name = "projectTab";
            this.projectTab.Padding = new System.Windows.Forms.Padding(3);
            this.projectTab.Size = new System.Drawing.Size(751, 652);
            this.projectTab.TabIndex = 0;
            this.projectTab.Text = "tabPage1";
            this.projectTab.UseVisualStyleBackColor = true;
            // 
            // addProjectButton
            // 
            this.addProjectButton.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.addProjectButton.Location = new System.Drawing.Point(0, 619);
            this.addProjectButton.Name = "addProjectButton";
            this.addProjectButton.Size = new System.Drawing.Size(751, 33);
            this.addProjectButton.TabIndex = 1;
            this.addProjectButton.Text = "Přidat projekt";
            this.addProjectButton.UseVisualStyleBackColor = true;
            this.addProjectButton.Click += new System.EventHandler(this.addProjectButton_Click);
            // 
            // projectList
            // 
            this.projectList.AllowUserToAddRows = false;
            this.projectList.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.projectList.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.projectList.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells;
            this.projectList.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.EnableAlwaysIncludeHeaderText;
            this.projectList.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.id,
            this.Projekt,
            this.deadline,
            this.priority});
            this.projectList.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.projectList.Location = new System.Drawing.Point(3, 3);
            this.projectList.Name = "projectList";
            this.projectList.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Sunken;
            this.projectList.Size = new System.Drawing.Size(745, 610);
            this.projectList.TabIndex = 0;
            // 
            // id
            // 
            this.id.HeaderText = "id";
            this.id.Name = "id";
            this.id.ReadOnly = true;
            this.id.Visible = false;
            // 
            // Projekt
            // 
            this.Projekt.HeaderText = "Projekt";
            this.Projekt.Name = "Projekt";
            this.Projekt.ReadOnly = true;
            // 
            // deadline
            // 
            this.deadline.HeaderText = "Deadline";
            this.deadline.Name = "deadline";
            this.deadline.ReadOnly = true;
            // 
            // priority
            // 
            this.priority.HeaderText = "Priority";
            this.priority.Name = "priority";
            this.priority.ReadOnly = true;
            // 
            // tabTasks
            // 
            this.tabTasks.Controls.Add(this.addTaskButton);
            this.tabTasks.Controls.Add(this.tasksList);
            this.tabTasks.Location = new System.Drawing.Point(4, 5);
            this.tabTasks.Name = "tabTasks";
            this.tabTasks.Padding = new System.Windows.Forms.Padding(3);
            this.tabTasks.Size = new System.Drawing.Size(751, 652);
            this.tabTasks.TabIndex = 1;
            this.tabTasks.Text = "tabPage1";
            this.tabTasks.UseVisualStyleBackColor = true;
            // 
            // addTaskButton
            // 
            this.addTaskButton.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.addTaskButton.Location = new System.Drawing.Point(6, 602);
            this.addTaskButton.Name = "addTaskButton";
            this.addTaskButton.Size = new System.Drawing.Size(739, 23);
            this.addTaskButton.TabIndex = 2;
            this.addTaskButton.Text = "Nová úloha";
            this.addTaskButton.UseVisualStyleBackColor = true;
            this.addTaskButton.Click += new System.EventHandler(this.addTaskButton_Click);
            // 
            // tasksList
            // 
            this.tasksList.AllowUserToAddRows = false;
            this.tasksList.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tasksList.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.tasksList.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells;
            this.tasksList.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.EnableAlwaysIncludeHeaderText;
            this.tasksList.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.task,
            this.dataGridViewTextBoxColumn3});
            this.tasksList.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.tasksList.Location = new System.Drawing.Point(3, 3);
            this.tasksList.Name = "tasksList";
            this.tasksList.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Sunken;
            this.tasksList.Size = new System.Drawing.Size(745, 593);
            this.tasksList.TabIndex = 1;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.HeaderText = "id";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.Visible = false;
            // 
            // task
            // 
            this.task.HeaderText = "Úloha";
            this.task.Name = "task";
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.HeaderText = "Projekt";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            // 
            // profileImage
            // 
            this.profileImage.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.profileImage.BackColor = System.Drawing.Color.Transparent;
            this.profileImage.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.profileImage.Location = new System.Drawing.Point(4, 586);
            this.profileImage.Name = "profileImage";
            this.profileImage.Size = new System.Drawing.Size(215, 32);
            this.profileImage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.profileImage.TabIndex = 3;
            this.profileImage.TabStop = false;
            // 
            // ProjectsWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(984, 661);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.leftControlPanel);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximumSize = new System.Drawing.Size(5000, 1200);
            this.MinimumSize = new System.Drawing.Size(1000, 700);
            this.Name = "ProjectsWindow";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "@user - Projektový manager";
            this.Load += new System.EventHandler(this.ProjectsWindow_Load);
            this.leftControlPanel.ResumeLayout(false);
            this.buttonsPanelControls.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.tabControl.ResumeLayout(false);
            this.projectTab.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.projectList)).EndInit();
            this.tabTasks.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tasksList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.profileImage)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel leftControlPanel;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.TabControl tabControl;
        private System.Windows.Forms.Panel buttonsPanelControls;
        private System.Windows.Forms.Button addProject;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TabPage projectTab;
        private System.Windows.Forms.TabPage tabTasks;
        private System.Windows.Forms.DataGridView projectList;
        private System.Windows.Forms.DataGridView tasksList;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn task;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.Button addProjectButton;
        private System.Windows.Forms.Button addTaskButton;
        private System.Windows.Forms.DataGridViewTextBoxColumn id;
        private System.Windows.Forms.DataGridViewTextBoxColumn Projekt;
        private System.Windows.Forms.DataGridViewTextBoxColumn deadline;
        private System.Windows.Forms.DataGridViewTextBoxColumn priority;
        private System.Windows.Forms.PictureBox profileImage;
    }
}