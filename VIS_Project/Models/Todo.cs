﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VIS_Project.Models
{
    class Todo
    {
        int id;
        String name;
        String description;
        DateTime? deadline;
        Status? status;
        Checklist checklist;
    }
}
