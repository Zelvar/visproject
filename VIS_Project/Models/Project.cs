﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VIS_Project.Models
{
    class Project
    {
        int id;
        /// <summary>
        /// Nazev
        /// </summary>
        String name;
        /// <summary>
        /// Popis projektu
        /// </summary>
        String description;
        /// <summary>
        /// Zadavatel
        /// </summary>
        User customer;                
        /// <summary>
        /// Deadline
        /// </summary>
        DateTime? deadline;
        /// <summary>
        /// Všechny pod-úlohy
        /// </summary>
        IList<Todo> Todos;
    }
}
