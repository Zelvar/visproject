﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VIS_Project.Models
{
    class ChecklistPoint
    {
        public String Name { get; set; }
        public Boolean Checked { get; set; }
    }
}
