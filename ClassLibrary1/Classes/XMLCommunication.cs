﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using System.IO;

namespace VIS_Project.Classes
{
    class XMLCommunication
    {
        XDocument docInstance;
        private static XMLCommunication __instance = null;
        private string XMLPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), Path.Combine("VISSettings", "storage.xml"));
        private string XMLPathWatcher = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "VISSettings");
        private int requestCounter = 0;

        private XMLCommunication() {
            docInstance = XDocument.Load(this.XMLPath);

            FileSystemWatcher fw = new FileSystemWatcher(this.XMLPathWatcher);
            fw.Changed += delegate {
                this.docInstance = null;
                this.docInstance = XDocument.Load(this.XMLPath);
            };
        }

        public XDocument getXML() {
            requestCounter++;
            return this.docInstance;
        }

        public static XMLCommunication getInstance()
        {
            if (__instance == null)
                __instance = new XMLCommunication();

            return __instance; 
        }

        public void save() {
            docInstance.Save(this.XMLPath);
        }
    }
}
