﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;

namespace VIS_Project.Classes
{
    interface IModel
    {
        void loadProperties(object o);
        object getInstance(string Type);
    }
}
