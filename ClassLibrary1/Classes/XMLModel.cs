﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.IO;
using System.Xml;
using System.Xml.Linq;
using System.Linq.Expressions;

namespace VIS_Project.Classes
{
    public class XMLModel : Model, IFacade
    {
        public object obj;
        object IFacade.obj { get { return this.obj; } set { this.obj = value; } }
        Type IFacade.type { get { return this.type; } set { this.type = value; } }

        //DONE
        public int create()
        {
            loadProperties(this.obj);

            if (PropertyValues.Count > 0) {

                List<string> fields = PropertyValues.Keys.ToList();
                List<string> fieldsvals = this.PropertyValues.Values.ToList();

                int pk_index = fields.IndexOf(primaryKeyColumn);
                int pk_index_data = PropertyTypes.IndexOf(primaryKeyColumn);

                fieldsvals[pk_index] = this.getNewAutoIncrement().ToString(); //GET 
                XElement element = new XElement(this.xmlTable);

                for (int i = 0; i < fieldsvals.Count; i++) {
                    element.Add(new XElement(fields[i], fieldsvals[i]));
                }
                
                XMLCommunication.getInstance().getXML().Element("data").Element(this.table).Add(element);
                XMLCommunication.getInstance().save();
            }

            return 0;
        }

        //Done
        public object[] read()
        {
            loadProperties(obj);
            IList<object> objects = new List<object>();
            IEnumerable<XElement> elements = XMLCommunication.getInstance().getXML().Element("data").Descendants(this.xmlTable);
            //Naplnění modelu
            foreach (var element in elements)
            {
                objects.Add(getInstance(type.ToString()));

                foreach (PropertyInfo pi in type.GetProperties())
                {
                    string propertyName = pi.Name;
                    if (pi != null && pi.CanWrite)
                    {
                        if (element.Element(propertyName) != null)
                        {
                            if (pi.PropertyType == typeof(string))
                            {
                                pi.SetValue(objects.Last(), element.Element(propertyName).Value);
                            }
                            if (pi.PropertyType == typeof(int))
                            {
                                pi.SetValue(objects.Last(), int.Parse(element.Element(propertyName).Value));
                            }
                        }
                    }
                }
            }

            PropertyValues.Clear();
            PropertyTypes.Clear();

            return objects.ToArray();
        }

        //DONE
        public object read(int id = 0)
        {
            loadProperties(this.obj);
            XElement element = XMLCommunication.getInstance().getXML().Element("data").Descendants(this.xmlTable).FirstOrDefault(x => x.Element("id").Value == id.ToString());
            if (element != null) {

                //Naplnění modelu
                foreach (PropertyInfo pi in type.GetProperties())
                {
                    string propertyName = pi.Name;
                    if (pi != null && pi.CanWrite)
                    {
                        if (element.Element(propertyName) != null)
                        {
                            if (pi.PropertyType == typeof(string))
                            {
                                pi.SetValue(this.obj, element.Element(propertyName).Value);
                            }
                            if (pi.PropertyType == typeof(int))
                            {
                                pi.SetValue(this.obj, int.Parse(element.Element(propertyName).Value));
                            }
                        }
                    }
                }

            }

            return new object();
        }

        //Done
        public object[] read(Dictionary<string, string> filter)
        {
            loadProperties(obj);
            IList<object> objects = new List<object>();

            if (filter.Count() > 0)
            {

                IEnumerable<XElement> elements = XMLCommunication.getInstance().getXML().Element("data").Descendants(this.xmlTable);
                if(elements.Count() > 0){
                    foreach (var param in filter)
                    {
                        elements = elements.Where(x => x.Element(param.Key).Value == param.Value);
                    }

                    Console.WriteLine(elements.Count());

                    //Naplnění modelu
                    foreach (var element in elements)
                    {
                        objects.Add(getInstance(type.ToString()));

                        foreach (PropertyInfo pi in type.GetProperties())
                        {
                            string propertyName = pi.Name;
                            if (pi != null && pi.CanWrite)
                            {
                                if (element.Element(propertyName) != null)
                                {
                                    if (pi.PropertyType == typeof(string))
                                    {
                                        pi.SetValue(objects.Last(), element.Element(propertyName).Value);
                                    }
                                    if (pi.PropertyType == typeof(int))
                                    {
                                        pi.SetValue(objects.Last(), int.Parse(element.Element(propertyName).Value));
                                    }
                                }
                            }
                        }
                    }
                }
            }

            PropertyValues.Clear();
            PropertyTypes.Clear();

            return objects.ToArray();
        }
        
        //DONE
        public int update(int id = 0)
        {
            loadProperties(this.obj);
            XElement element = XMLCommunication.getInstance().getXML().Element("data").Descendants(this.xmlTable).FirstOrDefault(x => x.Element("id").Value == id.ToString());
            if (element != null) {

                if (PropertyValues.Count > 0)
                {

                    List<string> fields = PropertyValues.Keys.ToList();
                    List<string> fieldsvals = this.PropertyValues.Values.ToList();

                    int pk_index = fields.IndexOf(primaryKeyColumn);
                    int pk_index_data = PropertyTypes.IndexOf(primaryKeyColumn);
                    fields.RemoveAt(pk_index);
                    fieldsvals.RemoveAt(pk_index);

                    PropertyTypes.RemoveAt(pk_index_data);
                    PropertyTypes.RemoveAt(pk_index_data);

                    for (int i = 0; i < fieldsvals.Count; i++)
                    {
                        element.Element(fields[i]).Value = fieldsvals[i];
                    }

                    //XMLCommunication.getInstance().getXML().Root.Add(element);
                    XMLCommunication.getInstance().save();
                }
            }

            return 0;
        }

        //DONE
        public int delete(int id = 0)
        {
            XElement element = XMLCommunication.getInstance().getXML().Element("data").Descendants(this.xmlTable).FirstOrDefault(x => x.Element("id").Value == id.ToString());
            if (element != null)
            {
                element.Remove();
                XMLCommunication.getInstance().save();

                return 1;
            }

            return 0;
        }

        //DONE
        public int getNewAutoIncrement() {
            return (XMLCommunication.getInstance().getXML().Element("data").Descendants(this.xmlTable).Count() > 0) ? (XMLCommunication.getInstance().getXML().Element("data").Element(this.table).Descendants(this.xmlTable).Max(x => (int)x.Element("id")) + 1) : 1;
        }

        public double getLast() {
            return this.getNewAutoIncrement()-1;
        }
    }
}
