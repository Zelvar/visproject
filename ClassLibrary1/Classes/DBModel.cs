﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;

namespace VIS_Project.Classes
{
    public class DBModel : Model, IFacade
    {
        private DBCommunication db;
        private object obj;

        object IFacade.obj { get { return this.obj; } set { this.obj = value; } }
        Type IFacade.type { get { return this.type; } set { this.type = value; } }

        public DBModel()
        {
            db = new DBCommunication();
            PropertyValues = new Dictionary<string, string>();
            PropertyTypes = new List<string>();
        }

        #region Other

        public double getLast() {
            return aggInit("SELECT max(id)" + " FROM " + this.table);;
        }

        private double aggInit(string sql)
        {
            string value = db.single(sql);

            double result;

            if (value != null && double.TryParse(value, out result))
            {
                return double.Parse(value);
            }

            return 0;
        }

        #endregion
        
        #region CREATE
        //CREATE (D)
        public int create()
        {
            loadProperties(obj);

            if (PropertyValues.Count > 0)
            {
                List<string> fields = PropertyValues.Keys.ToList();
                List<string> fieldsvals = this.PropertyValues.Values.ToList();
                
                int pk_index = fields.IndexOf(primaryKeyColumn);
                int pk_index_data = PropertyTypes.IndexOf(primaryKeyColumn);

                // Remove the primary key, we don't need that in an insert query
                fields.RemoveAt(pk_index);
                fieldsvals.RemoveAt(pk_index);

                PropertyTypes.RemoveAt(pk_index_data);
                PropertyTypes.RemoveAt(pk_index_data);

                string sFields = String.Join(",", fields);

                for (int i = 0; i < fieldsvals.Count; i++)
                {
                    fields[i] = "@" + fields[i];
                }

                string sql = "INSERT INTO " + this.table + "(" + sFields + ")" + " VALUES(" + String.Join(",", fields) + ")";
                int result = this.db.nQuery(sql, PropertyTypes.ToArray());

                PropertyValues.Clear();
                PropertyTypes.Clear();

                return result;
            }
            else
            {
                string sql = "INSERT INTO " + this.table + "()" + " VALUES()";
                return this.db.nQuery(sql);
            }
        }
        #endregion
        #region Read
        //READ (D)
        public object[] read()
        {
            loadProperties(obj);
            IList<object> objects = new List<object>();

            DataTable d = db.table(this.table);
            IList<Dictionary<string, string>> rows = new List<Dictionary<string, string>>();

            //Načtení jednotlivých sloupců
            if (d.Rows.Count > 0)
            {
                for (int i = 0; i < d.Rows.Count; i++)
                {
                    Dictionary<string, string> row = new Dictionary<string, string>();
                    for (int j = 0; j < d.Columns.Count; j++)
                    {
                        row.Add(d.Columns[j].ColumnName.ToLower(), d.Rows[i][j].ToString());
                    }
                    rows.Add(row);
                }
            }
                
            //Naplnění modelu
            foreach(var row in rows){
                objects.Add(getInstance(type.ToString()));
                foreach (PropertyInfo pi in type.GetProperties())
                {
                    string propertyName = pi.Name;
                    if (pi != null && pi.CanWrite)
                    {
                        if (row.ContainsKey(propertyName))
                        {
                            if (pi.PropertyType == typeof(string))
                            {
                                pi.SetValue(objects.Last(), row[propertyName]);
                            }
                            if (pi.PropertyType == typeof(int))
                            {
                                pi.SetValue(objects.Last(), int.Parse(row[propertyName]));
                            }
                        }
                    }
                }
            }
            PropertyValues.Clear();
            PropertyTypes.Clear();

            return objects.ToArray();

        }
        //READ by Primary Key (D)
        public object read(int id = 0)
        {
            loadProperties(obj);

            if (id != 0)
            {
                //Vygenerování SQL dotazu
                string sql = "SELECT * FROM " + this.table + " WHERE " + this.primaryKeyColumn + " = @id LIMIT 1";

                //Načtení DAT
                DataTable d = this.db.query(sql, new string[] { "id", id.ToString() });
                Dictionary<string, string> row = new Dictionary<string, string>();

                //Načtení jednotlivých sloupců
                if (d.Rows.Count > 0)
                {
                    for (int i = 0; i < d.Columns.Count; i++)
                    {
                        row.Add(d.Columns[i].ColumnName.ToLower(), d.Rows[0][i].ToString());
                        new Debug(d.Columns[i].ColumnName.ToString());
                    }
                }

                //Naplnění modelu
                foreach (PropertyInfo pi in type.GetProperties())
                {
                    string propertyName = pi.Name;
                    if (pi != null && pi.CanWrite)
                    {
                        if (row.ContainsKey(propertyName))
                        {
                            if (pi.PropertyType == typeof(string))
                            {
                                pi.SetValue(this.obj, row[propertyName]);
                            }
                            if (pi.PropertyType == typeof(int))
                            {
                                pi.SetValue(this.obj, int.Parse(row[propertyName]));
                            }
                        }
                    }
                }
            }
            PropertyValues.Clear();
            PropertyTypes.Clear();

            return this;
        }
        //READ by par and val
        public object[] read(Dictionary<string, string> filter)
        {
            loadProperties(obj);
            IList<object> objects = new List<object>();

            if (filter.Count() > 0)
            {
                //Vygenerování SQL dotazu                
                List<string> parameters = new List<string>();
                List<string> parametersQuery = new List<string>();
                foreach (var filterParams in filter){
                    parameters.Add(filterParams.Key.ToLower());
                    parameters.Add(filterParams.Value);

                    parametersQuery.Add(filterParams.Key.ToLower() + " = " + "@"+filterParams.Key.ToLower());
                }

                string sql = "SELECT * FROM " + this.table + " WHERE " + String.Join(" AND ", parametersQuery.ToArray());
                //Načtení DAT
                DataTable d = this.db.query(sql, parameters.ToArray());
                IList<Dictionary<string, string>> rows = new List<Dictionary<string, string>>();

                //Načtení jednotlivých sloupců
                if (d.Rows.Count > 0)
                {
                    for (int i = 0; i < d.Rows.Count; i++)
                    {
                        Dictionary<string, string> row = new Dictionary<string, string>();
                        for (int j = 0; j < d.Columns.Count; j++)
                        {
                            row.Add(d.Columns[j].ColumnName.ToLower(), d.Rows[i][j].ToString());
                        }
                        rows.Add(row);
                    }
                }
                
                //Naplnění modelu
                foreach(var row in rows){
                    objects.Add(getInstance(type.ToString()));
                    foreach (PropertyInfo pi in type.GetProperties())
                    {
                        string propertyName = pi.Name;
                        if (pi != null && pi.CanWrite)
                        {
                            if (row.ContainsKey(propertyName))
                            {
                                if (pi.PropertyType == typeof(string))
                                {
                                    pi.SetValue(objects.Last(), row[propertyName]);
                                }
                                if (pi.PropertyType == typeof(int))
                                {
                                    pi.SetValue(objects.Last(), int.Parse(row[propertyName]));
                                }
                            }
                        }
                    }
                }
            }
            PropertyValues.Clear();
            PropertyTypes.Clear();

            return objects.ToArray();
        }
        
        #endregion
        #region Update
        //Update (D)
        public int update(int id = 0)
        {
            loadProperties(obj);

            if (this.PropertyValues.Count > 0)
            {
                if (id != 0)
                {
                    this.PropertyValues[this.primaryKeyColumn] = id.ToString();
                }

                string update = string.Empty;

                foreach (string column in this.PropertyValues.Keys)
                {
                    if (column != this.primaryKeyColumn)
                        update += column + " =@" + column + " , ";
                }

                update = update.Trim();
                update = update.Substring(0, update.Length - 1);

                string sql = "UPDATE " + this.table + " SET " + update + " WHERE " + primaryKeyColumn + "= @id LIMIT 1";

                int pk_index = PropertyTypes.IndexOf(primaryKeyColumn);

                PropertyTypes.RemoveAt(pk_index);
                PropertyTypes.RemoveAt(pk_index);

                PropertyTypes.Add("@id");
                PropertyTypes.Add(PropertyValues[primaryKeyColumn]);
                
                return db.nQuery(sql, PropertyTypes.ToArray());
            }

            return 0;
        }
        #endregion
        #region Delete
        //Delete (D)
        public int delete(int id = 0)
        {
            if (id >= 1)
            {
                string sql = "DELETE FROM " + this.table + " WHERE " + this.primaryKeyColumn + " = @id LIMIT 1";
                return this.db.nQuery(sql, new string[] { "id", id.ToString() });
            }
            return 0;
        }
        #endregion
    }
}
