﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using MySql.Data;
using MySql.Data.MySqlClient;

namespace VIS_Project.Classes
{
    public class DBCommunication
    {
        //Připojení, Command, Reader
        private MySqlConnection connect;
        private MySqlCommand command;
        private MySqlDataReader reader;
        
        //Pro kontrolu připojení
        private bool Connected = false;

        private DataTable Table;
        private int affected_rows;
        private string squery;

        //Parametry pro query
        private List<string> parameters;

        public DBCommunication()
        {
            Connect();
            Table = new DataTable();
            parameters = new List<string>();
        }

        private void Connect()
        {
            connect = DBConnect.getInstance().getConnection();
            Connected = true;
        }

        private void Init(string Query, string[] bindings = null)
        {
            if (Connected == false) {
                Connect();
            }

            using (command = new MySqlCommand(Query, connect))
            {
                bind(bindings);

                if (parameters.Count > 0){
                    parameters.ForEach(delegate(string parameter)
                    {
                        string[] sparameters = parameter.ToString().Split('\x7F');
                        command.Parameters.AddWithValue(sparameters[0], sparameters[1]);
                    });
                }

                this.squery = Query.ToLower();

                if (squery.Contains("select")){
                    this.Table = execDatatable();
                }else if (squery.Contains("delete") || squery.Contains("update") || squery.Contains("insert")){
                    this.affected_rows = execNonquery();
                }

                this.parameters.Clear();
            }
        }

        public void bind(string field, string value){
            parameters.Add("@" + field + "\x7F" + value);
        }

        //Bind fields
        public void bind(string[] f){
            if (f != null){
                for (int i = 0; i < f.Length; i++){
                    bind(f[i], f[i + 1]);
                    i += 1;
                }
            }
        }

        // Example:
        // qBind(new string[] { "12", "John" });
        // nQuery("SELECT * FROM User WHERE ID=@0 AND Name=@1");
        /// <summary>
        /// Bind multiple fields/values without identifier.
        /// </summary>
        /// <param name="fields"></param>
        public void qBind(string[] fields)
        {
            if (fields != null)
            {
                for (int i = 0; i < fields.Length; i++)
                {
                    bind(i.ToString(), fields[i]);
                }
            }
        }

        private DataTable execDatatable()
        {
            DataTable dt = new DataTable();
            try
            {
                using (MySqlDataReader reader = command.ExecuteReader())
                {
                    dt.Load(reader);
                }
            }
            catch (Exception my)
            {
                string exception = "Exception : " + my.Message.ToString() + "\n\r SQL Query : \n\r" + squery;
                new Debug(exception);
            }

            return dt;
        }

        private int execNonquery()
        {

            int affected = 0;

            try{
                affected = command.ExecuteNonQuery();
            }
            catch (MySqlException my){
                string exception = "Exception : " + my.Message.ToString() + "\n\r SQL Query : \n\r" + squery;
                new Debug(exception);
            }

            return affected;
        }


        public DataTable table(string table, string[] bindings = null)
        {
            Init("SELECT * FROM " + table, bindings);
            return this.Table;
        }

        public DataTable query(string query, string[] bindings = null){
            Init(query, bindings);
            return this.Table;
        }

        public int nQuery(string query, string[] bindings = null){
            Init(query, bindings);
            return this.affected_rows;
        }

        public string single(string query, string[] bindings = null){
            Init(query, bindings);

            if (Table.Rows.Count > 0)
                return Table.Rows[0][0].ToString();

            return string.Empty;
        }

        public string[] row(string query, string[] bindings = null){
            Init(query, bindings);

            string[] row = new string[Table.Columns.Count];

            if (Table.Rows.Count > 0)
                for (int i = 0; i++ < Table.Columns.Count; row[i - 1] = Table.Rows[0][i - 1].ToString()) ;

            return row;
        }

        public List<string> column(string query, string[] bindings = null){

            Init(query, bindings);

            List<string> column = new List<string>();
            for (int i = 0; i++ < Table.Rows.Count; column.Add(Table.Rows[i - 1][0].ToString()));

            return column;
        }
    }
}
