﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using MySql.Data;
using MySql.Data.MySqlClient;

namespace VIS_Project.Classes
{
    public class DBConnect
    {
        private string srv = "127.0.0.1";
        private string database = "vis";
        private string user = "root";
        private string pwd = "root";
        private MySqlConnection connect;
        private bool bConnected = false;

        private static DBConnect __instance = null;

        private DBConnect()
        {
            Connect();
        }

        private void Connect()
        {
            try
            {
                MySqlConnection connection = new MySqlConnection(String.Format("Database={0};DataSource={1};UserID={2};Password={3}", database, srv, user, pwd));
                connection.Open();
                connect = connection;
                bConnected = true;
            }
            catch (MySqlException ex)
            {
                string exception = "Exception : " + ex.Message.ToString() + "\n\rApplication will close now. \n\r";
                new Debug(exception);

                //Environment.Exit(1);
            }
        }

        public MySqlConnection getConnection() {
            if (!bConnected)
                Connect();
            return connect;
        }

        public static DBConnect getInstance() { 
            if(__instance == null)
                __instance = new DBConnect();

            return __instance; 
        }

        public void CloseConn()
        {
            bConnected = false;
            connect.Close();
            connect.Dispose();
        }
    }
}
