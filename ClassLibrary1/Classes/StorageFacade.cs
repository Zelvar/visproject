﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace VIS_Project.Classes
{
    /// <summary>
    /// Datová vrstva pro obsluhu databáze a xml
    /// Obsahuje základní CRUD operace nad objekty
    /// </summary>
    public class StorageFacade
    {
        /// <summary>
        /// Základní proměnné fasády
        /// </summary>
        private bool sql = false;
        private IFacade CommunicationModel = null;

        private object obj = null;
        private Type type = null;

        /// <summary>
        /// Cesta k souboru s nastavení v %appdata%/VISSettings/settings.ini
        /// </summary>
        private string configPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "VISSettings/settings.ini");
        /// <summary>
        /// Instance objektu fasády (Singleton)
        /// </summary>
        private static StorageFacade __instance = null;
        /// <summary>
        /// Získání instance objektu
        /// </summary>
        /// <returns>
        /// Vrací instanci objektu typu StorageFacede
        /// </returns>
        public static StorageFacade getInstance(){
            if (__instance == null)
                __instance = new StorageFacade();

            return __instance;
        }
        /// <summary>
        /// Slouží k nastavení objektu nad kterými probíhají CRUD operace
        /// </summary>
        /// <param name="obj">
        /// Daný objekt
        /// </param>
        /// <returns></returns>
        public StorageFacade setObject(object obj) {
            this.obj = obj;
            this.type = obj.GetType();
            return this;
        }
        /// <summary>
        /// Možnost natvrdo nastavit typ ukládani
        /// </summary>
        public string storageType {
            get {
                return (sql ? "SQL" : "XML");
            }
            set {
                if (value.ToLower() == "sql")
                {
                    sql = true;
                }
                else {
                    sql = false;
                }

                this.createInstance();
            }
        }
        private StorageFacade() {
            if (File.Exists(configPath))
            {
                INIParser parser = new INIParser(configPath);
                sql = bool.Parse(parser.IniReadValue("storage", "Database"));
            }
            
            this.createInstance();
        }
        /// <summary>
        /// Slouží k internímu vytvoření instance
        /// </summary>
        private void createInstance(){
            if (sql && CommunicationModel == null) {
                CommunicationModel = new DBModel();
            }

            if (!sql && CommunicationModel == null)
            {
                CommunicationModel = new XMLModel();
            }
        }

        public int getLast()
        {
            CommunicationModel.obj = this.obj;
            CommunicationModel.type = this.type;
            return Convert.ToInt32(CommunicationModel.getLast());    
        }

        #region CRUD (Create, Read, Update, Delete)
        //DONE SQL / XML
        public void create()
        {
            CommunicationModel.obj = this.obj;
            CommunicationModel.type = this.type;
            CommunicationModel.create();
        }

        /*public int createMore(List<object> obj)
        {
            throw new NotImplementedException();
        }

        public int createMore(object[] obj)
        {
            throw new NotImplementedException();
        }*/

        //DONE
        public object[] read()
        {
            CommunicationModel.obj = this.obj;
            CommunicationModel.type = this.type;
            return CommunicationModel.read();
        }

        //DONE SQL / XML
        public void read(int id = 0)
        {
            CommunicationModel.obj = this.obj;
            CommunicationModel.type = this.type;
            this.obj = CommunicationModel.read(id);
        }
        //DONE SQL / XML
        public object[] read(Dictionary<string, string> filter)
        {
            CommunicationModel.obj = this.obj;
            CommunicationModel.type = this.type;
            return CommunicationModel.read(filter);
        }

        //DONE SQL / XML
        public void update(int id = 0)
        {
            CommunicationModel.obj = this.obj;
            CommunicationModel.type = this.type;
            this.obj = CommunicationModel.update(id);
        }

        //DONE SQL / XML
        public void delete(int id = 0)
        {
            CommunicationModel.obj = this.obj;
            CommunicationModel.type = this.type;
            this.obj = CommunicationModel.delete(id);
        }
        #endregion
    }
}
