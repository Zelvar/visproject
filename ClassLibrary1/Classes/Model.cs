﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;

namespace VIS_Project.Classes
{
    public class Model : IModel
    {
        //Vyplněno v jednotlivých modelech

        /// <summary>
        /// Table name (projects, Todos etc.)
        /// </summary>
        public string table;
        public string primaryKeyColumn;
        /// <summary>
        /// Xml base Element name (project, todo, status etc.)
        /// </summary>
        public string xmlTable;

        //Mezi paměť
        protected List<string> PropertyTypes;
        protected Dictionary<string, string> PropertyValues;

        //Objekt
        protected object obj;
        public Type type;

        public Model() {
            PropertyValues = new Dictionary<string, string>();
            PropertyTypes = new List<string>();
        }

        //Načteni dat z modelu (D)
        public void loadProperties(object o)
        {
            PropertyValues.Clear();
            PropertyTypes.Clear();

            foreach (PropertyInfo pi in o.GetType().GetProperties())
            {
                Console.Write(pi.Name.ToString());
                if (pi.Name != "table" && pi.Name != "primaryKeyColumn" && pi.Name != "xmlTable")
                {
                    object property = pi.GetValue(o, null);
                    if (property != null)
                    {
                        this.PropertyValues[pi.Name] = property.ToString();

                        this.PropertyTypes.Add(pi.Name);
                        this.PropertyTypes.Add(property.ToString());
                    }
                    Console.WriteLine(pi.Name == "table");
                }
                else
                {
                    object property = pi.GetValue(o, null);
                    if (property != null && pi.Name == "table")
                    {
                        this.table = property.ToString();
                    }
                    if (property != null && pi.Name == "primaryKeyColumn")
                    {
                        this.primaryKeyColumn = property.ToString();
                    }
                    if (property != null && pi.Name == "xmlTable")
                    {
                        this.xmlTable = property.ToString();
                    }
                }
            }
        }

        public object getInstance(string type)
        {
            return (object)Activator.CreateInstance(Type.GetType(type));
        }
    }
}
