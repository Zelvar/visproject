﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;

namespace VIS_Project.Classes
{
    interface IFacade
    {
        object obj { get; set; }
        Type type { get; set; }

        int create();
        object[] read();
        object read(int id = 0);
        object[] read(Dictionary<string, string> filter);
        int update(int id = 0);
        int delete(int id = 0);
        double getLast();
    }
}
