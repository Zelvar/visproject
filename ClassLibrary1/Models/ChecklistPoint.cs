﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VIS_Project.Classes;

namespace VIS_Project.Models
{
    public class ChecklistPoint : Model
    {
        public int id { get; set; }
        public string name { get; set; }
        public int isChecked { get; set; }
        
        public string table { get { return "checklists_point"; } }
        public string xmlTable { get { return "checklist_point"; } }
        public string primaryKeyColumn { get { return "id"; } }
    }
}
