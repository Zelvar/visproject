﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VIS_Project.Classes;

namespace VIS_Project.Models
{
    public class ProjectRights : Model
    {
        public int project_id { get; set; }
        public int user_id { get; set; }
        public int id { get; set; }

        public string table { get { return "projects_rights"; } }
        public string xmlTable { get { return "project_right"; } }
        public string primaryKeyColumn { get { return "id"; } }
    }
}
