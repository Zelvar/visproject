﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VIS_Project.Classes;

namespace VIS_Project.Models
{
    public class Project : Model
    {
        public int id { get; set; }
        /// <summary>
        /// Nazev
        /// </summary>
        public string name { get; set; }
        /// <summary>
        /// Popis projektu
        /// </summary>
        public string description { get; set; }

        public int priority { get; set; }
        public string deadlinetimestamp
        {
            get {
                return this.deadline.ToString();
            }
            set
            {
                DateTime.TryParse(value, out DateTime result);
                this.deadline = result;
            }
        }

        /// <summary>
        /// Zadavatel
        /// </summary>
        User customer { get; set; }             
        /// <summary>
        /// Deadline
        /// </summary>
        DateTime deadline { get; set; }
        /// <summary>
        /// Všechny pod-úlohy projektu
        /// </summary>
        List<object> todos = new List<object>();

        public string table { get { return "projects"; } }
        public string xmlTable { get { return "project"; } }
        public string primaryKeyColumn { get { return "id"; } }

        public Project() { this.deadline = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc); }

        public override string ToString()
        {
            return id.ToString() + ":" + name + " Popis: " + description;
        }

        public DateTime getDeadLine(){
            return this.deadline;
        }

        public List<object> getTodos() {
            Dictionary<string, string> filter = new Dictionary<string, string>();
            filter.Add("project", this.id.ToString());
            this.todos = StorageFacade.getInstance().setObject(new Todo()).read(filter).Cast<object>().ToList();

            return this.todos;
        }

        public void addTodo(Todo todo) {
            this.todos.Add(todo);
        }

        public List<object> getUsers()
        {
            Dictionary<string, string> filter = new Dictionary<string, string>();
            List<object> users = new List<object>();
            
            filter.Add("project_id", this.id.ToString());

            foreach (ProjectRights pr in StorageFacade.getInstance().setObject(new ProjectRights()).read(filter).Cast<object>().ToList())
            {
                var user = new User()
                {
                    id = pr.user_id
                };
                StorageFacade.getInstance().setObject(user).read(user.id);

                users.Add( user );
            }

            return users;
        }

        public void save()
        {
            StorageFacade.getInstance().setObject(this).update(this.id);
        }
    }
}
