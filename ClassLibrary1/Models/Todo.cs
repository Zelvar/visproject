﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VIS_Project.Classes;

namespace VIS_Project.Models
{
    public class Todo : Model
    {
        public int id { get; set; }
        public string name { get; set; }
        public string description { get; set; }

        public int time { get; set; }
        public int project { get; set; }

        DateTime? deadline { get; set; }
        Status? status { get; set; }
        Checklist checklist { get; set; }

        public string table { get { return "todos"; } }
        public string xmlTable { get { return "todo"; } }
        public string primaryKeyColumn { get { return "id"; } }

        public Project getProject() { 
            Project project = new Project();

            if(this.project > 0)
                StorageFacade.getInstance().setObject(project).read(this.project);

            return project;
        }
    }
}
