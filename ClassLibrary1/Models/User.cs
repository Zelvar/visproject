﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VIS_Project.Classes;

namespace VIS_Project.Models
{
    public class User : Model
    {
        private Position post = Position.User;
        private List<object> rights = new List<object>();
        private List<object> projects = new List<object>();
        private List<object> todos = new List<object>();

        public int id { get; set; }
        public string username { get; set; }
        public string password { get; set; }

        public int position {
            set { this.post = (Position)value; } 
            get { return (int)post; }
        }

        public string table { get { return "users"; } }
        public string xmlTable { get { return "user"; } }
        public string primaryKeyColumn { get { return "id"; } }

        /// <summary>
        /// Získání listu projektu uživatele
        /// </summary>
        /// <returns></returns>
        public List<object> getProjects(bool reload = false)
        {
            if (reload)
                this.projects.Clear();

            if (this.projects.Count <= 0 && this.post == Position.Admin)
            {
                this.projects.Clear();
                this.projects = StorageFacade.getInstance().setObject(new Project()).read().Cast<object>().ToList();
            }
            if (this.projects.Count <= 0 && this.post == Position.User)
            {
                this.projects.Clear();
                Dictionary<string, string> filter = new Dictionary<string, string>();
                filter.Add("user_id", this.id.ToString());
                this.rights = StorageFacade.getInstance().setObject(new ProjectRights()).read(filter).Cast<object>().ToList();

                foreach (ProjectRights right in rights)
                {
                    Project pr = new Project();
                    StorageFacade.getInstance().setObject(pr).read(right.project_id);
                    this.projects.Add((object)pr);
                }
            }

            return this.projects;
        }

        /// <summary>
        /// Získání projektu podle ID
        /// </summary>
        public Project getProject(int id)
        {
            Project project = new Project();
            StorageFacade.getInstance().setObject(project).read(id);

            return project;
        }

        /// <summary>
        /// Získání tasku podle ID
        /// </summary>
        public Todo getTask(int id)
        {
            Todo todo = new Todo();
            StorageFacade.getInstance().setObject(todo).read(id);
            return todo;
        }

        /// <summary>
        /// Získání Listu s TODO uživatele
        /// </summary>
        /// <returns></returns>
        public List<object> getTodos(bool reload = false)
        {
            if (reload)
                this.todos.Clear();

            if (this.todos.Count <= 0 && this.post == Position.Admin)
            {
                this.todos = StorageFacade.getInstance().setObject(new Todo()).read().Cast<object>().ToList();
            }
            if (this.todos.Count <= 0 && this.post == Position.User)
            {
                this.getProjects();
                foreach (Project project in projects) {
                    todos.Concat(project.getTodos());
                }
            }

            return this.todos;
        }

        /// <summary>
        /// Funkce pro vytvoření projektu, zároveň vytváří práva pro uživatele a přidáva je do listu projektu instance
        /// </summary>
        /// <param name="project">Nový projekt pro vložení do DB</param>
        public void createProject(Project project) {
            StorageFacade.getInstance().setObject(project).create();
            StorageFacade.getInstance().setObject(project).read(StorageFacade.getInstance().setObject(project).getLast());

            StorageFacade.getInstance().setObject(new ProjectRights() {
                project_id = project.id,
                user_id = this.id
            }).create();

            projects.Add(project);
        }

        public Position getPosition() {
            return this.post;
        }
    }
}
