﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VIS_Project.Classes;

namespace VIS_Project.Models
{
    public class Checklist : Model
    {
        public int ID { get; set; }
        IList<ChecklistPoint> checklist;

        public string table { get { return "checklists"; } }
        public string xmlTable { get { return "checklist"; } }
        public string primaryKeyColumn { get { return "id"; } }
    }
}
